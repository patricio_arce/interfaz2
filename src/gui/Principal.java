package gui;

import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Principal extends JFrame {

	private JPanel contentPane;
	
	// Vistas de usuario
	private LoginPanel loginPanel;
	private HomePanel homePanel;
	private RegistroPanel registroPanel;
	private EntradaPanel entradaPanel;
	private SalidaPanel salidaPanel;
	
	public static final int WIDTH = 350;
	public static final int HEIGHT = 600;
	

	/**
	 * Launch the application.
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Principal frame = new Principal();
		frame.setVisible(true);
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 */
	public Principal() throws IOException {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, WIDTH, HEIGHT);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		// Se reserva memoria a las Vistas de usuario (JPanel)
		loginPanel = new LoginPanel(this);
		contentPane.add(loginPanel);
		
		homePanel = new HomePanel(this);
		contentPane.add(homePanel);
		
		registroPanel = new RegistroPanel(this);
		contentPane.add(registroPanel);
		
		entradaPanel = new EntradaPanel(this);
		contentPane.add(entradaPanel);
		
		salidaPanel = new SalidaPanel(this);
		contentPane.add(salidaPanel);
		//Vista de usuario por defecto
		verLoginPanel();
	}
	
	// Metodos de navegaciom
	public void verLoginPanel() {
		homePanel.setVisible(false);
		registroPanel.setVisible(false);
		entradaPanel.setVisible(false);
		salidaPanel.setVisible(false);
		loginPanel.init();
	}
	public void verHomePanel() {
		loginPanel.setVisible(false);
		registroPanel.setVisible(false);
		entradaPanel.setVisible(false);
		salidaPanel.setVisible(false);
		homePanel.init();
	}
	public void verRegistroPanel() {
		loginPanel.setVisible(false);
		homePanel.setVisible(false);
		entradaPanel.setVisible(false);
		salidaPanel.setVisible(false);
		registroPanel.init();
	}
	public void verEntradaPanel() {
		loginPanel.setVisible(false);
		homePanel.setVisible(false);
		salidaPanel.setVisible(false);
		registroPanel.setVisible(false);
		entradaPanel.init();
	}
	public void verSalidaPanel() {
		loginPanel.setVisible(false);
		homePanel.setVisible(false);
		entradaPanel.setVisible(false);
		registroPanel.setVisible(false);
		salidaPanel.init();
	}
}
