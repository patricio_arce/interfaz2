package gui;

import javax.swing.JPanel;
import javax.swing.JTextField;

import cl.ufro.controller.UsuarioController;
import cl.ufro.db.Usuario;
import cl.ufro.utils.Imagen;
import cl.ufro.utils.ManagerSession;
import cl.ufro.utils.Serializar;
import cl.ufro.utils.PanelImagen;

import javax.swing.JLabel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;

public class SalidaPanel extends VistaUsuarioPanel {
	
	private Usuario user;
	private PanelImagen panelImagen;
	private JTextField textField;
	private JLabel lblNombreusuario;
	
	public SalidaPanel(Principal principal) {
		super(principal);
		build();
	}
	
	@Override
	protected void build() {
		setLayout(null);
		setBounds(0, 0, Principal.WIDTH, Principal.HEIGHT);
		/*
		JLabel lblLeer = new JLabel("Lea el qr para registrar la entrada al establecimiento");
		lblLeer.setBounds(48, 83, 272, 14);
		add(lblLeer);
		*/
		panelImagen = new PanelImagen();
		panelImagen.setBounds(142, 139, 150, 99);
		add(panelImagen);
		
		/*
		JButton btnCargarQr = new JButton("Cargar QR");
		btnCargarQr.setBounds(24, 120, 89, 23);
		btnCargarQr.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser file = new JFileChooser("./");
				file.showOpenDialog(null);
				File arch = file.getSelectedFile();
				if(arch!=null){
					BufferedImage imgOrig =Imagen.readColorImage(arch.getAbsolutePath());
					BufferedImage imgEscalada = Imagen.scale(imgOrig, panelImagen.getWidth(), panelImagen.getHeight());
					panelImagen.setImage(imgEscalada);	
					byte[] byteFoto = Serializar.BufferedImageToBytes(imgOrig);
					ManagerSession.saveObject("byteFoto", byteFoto);
				}
			}
		});
		add(btnCargarQr);
		*/
		
		JButton btnRegistrarEntrada = new JButton("Registrar entrada");
		btnRegistrarEntrada.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String rut= textField.getText();
				UsuarioController usuarioController = new UsuarioController();
				usuarioController.salida(user, rut);
			}
		});
		btnRegistrarEntrada.setBounds(48, 265, 117, 23);
		add(btnRegistrarEntrada);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setBounds(193, 265, 89, 23);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ManagerSession.saveObject("usuario", user);
				principal.verHomePanel();	
			}
		});
		add(btnVolver);
		
		JLabel lblIngreseRutDel = new JLabel("Ingrese rut del establecimeinto");
		lblIngreseRutDel.setBounds(61, 74, 244, 14);
		add(lblIngreseRutDel);
		
		textField = new JTextField();
		textField.setBounds(35, 104, 270, 20);
		add(textField);
		textField.setColumns(10);
		
		
		
	}
	
	public void init() {
		Usuario usuario = (Usuario)ManagerSession.findObject("usuario");
		user.setNombre(usuario.getNombre());
		user.setRut(usuario.getRut());
		user.setClave(usuario.getClave());
		user.setCorreo(usuario.getCorreo());
		setVisible(true);
	}
}
