package gui;

import java.io.IOException;

import javax.swing.JPanel;

public abstract class VistaUsuarioPanel extends JPanel {

	protected Principal principal;

	public VistaUsuarioPanel(Principal principal) {
		this();
		this.principal = principal;
	}
	public VistaUsuarioPanel() {
		setLayout(null);
		setBounds(0, 0, Principal.WIDTH, Principal.HEIGHT);

	}
	public abstract void init();
	protected abstract void build() throws IOException;
}
