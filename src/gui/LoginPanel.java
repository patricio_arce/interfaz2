package gui;

import javax.swing.JPanel;
import javax.swing.JTextField;



import javax.swing.ImageIcon;
import java.awt.Image;
import javax.imageio.ImageIO;
import cl.ufro.controller.UsuarioController;
import cl.ufro.db.Usuario;
import cl.ufro.utils.Imagen;
import cl.ufro.utils.ManagerSession;
import cl.ufro.utils.PanelImagen;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.Color;

public class LoginPanel extends VistaUsuarioPanel  {

	private JTextField txtRut;
	private JTextField txtClave;
	private JLabel lblMensaje;
	private JLabel lblRegistro;
	private PanelImagen panelImagen;
	
	/**
	 * Create the panel.
	 * @throws IOException 
	 */
	public LoginPanel(Principal principal) throws IOException {
		super(principal);
		build();
	}
	@Override
	protected void build() throws IOException {
		JLabel lblRut = new JLabel("Rut:");
		lblRut.setBounds(83, 196, 70, 15);
		add(lblRut);
		
		JLabel lblClave = new JLabel("Clave:");
		lblClave.setBounds(83, 223, 70, 15);
		add(lblClave);
		
		lblMensaje = new JLabel("Mensaje");
		lblMensaje.setBounds(83, 299, 184, 15);
		add(lblMensaje);
		
		txtRut = new JTextField();
		txtRut.setBounds(141, 194, 114, 19);
		add(txtRut);
		txtRut.setColumns(10);
		
		txtClave = new JTextField();
		txtClave.setBounds(141, 221, 114, 19);
		add(txtClave);
		txtClave.setColumns(10);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//conectar cuenta
				
				Usuario usuario = new Usuario();
				usuario.setRut(txtRut.getText());
				usuario.setClave(Integer.parseInt(txtClave.getText()));
				
				UsuarioController usuarioController = new UsuarioController();
				usuario = usuarioController.identificar(usuario);
				if(usuario==null)
					lblMensaje.setText("Usurio inv�lido!!");
				else {
					//lblMensaje.setText("Hola "+usuario.getNombre());
					ManagerSession.saveObject("usuario", usuario);
					principal.verHomePanel();
					
				}
			}
		});
		btnEntrar.setBounds(36, 264, 117, 25);
		add(btnEntrar);
		
		
		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(191, 265, 114, 23);
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {					
				principal.verRegistroPanel();
			}
		});
		add(btnRegistrar);
		
		// IMPLEMENTACION DE CARGAR IMAGEN
		
		panelImagen = new PanelImagen();
		panelImagen.setBounds(105, 344, 130, 131);
		add(panelImagen);
		BufferedImage imgOrig = ImageIO.read(new File(".\\src\\Archivo\\123456789.PNG"));
		//JLabel picLabel = new JLabel(new ImageIcon(myPicture));
		BufferedImage imgEscalada = Imagen.scale(imgOrig, panelImagen.getWidth(), panelImagen.getHeight());
		panelImagen.setImage(imgEscalada);
		
		
	}
	public void init() {
		lblMensaje.setText("");
		txtRut.setText("");
		txtClave.setText("");
		setVisible(true);
	}
	
}
