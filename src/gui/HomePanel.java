package gui;

import javax.swing.JPanel;

import cl.ufro.db.Usuario;
import cl.ufro.utils.ManagerSession;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class HomePanel extends VistaUsuarioPanel {
	
	private JLabel lblNombreUsuario;
	private Usuario user;
	
	/**
	 * Create the panel.
	 */
	public HomePanel(Principal principal) {
		super(principal);
		build();
	}
	@Override
	protected void build() {
		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ManagerSession.removeObject("usuario");
				principal.verLoginPanel();
			}
		});
		btnVolver.setBounds(181, 246, 120, 98);
		add(btnVolver);
		
		JLabel lblHola = new JLabel("Hola ");
		lblHola.setBounds(22, 17, 45, 15);
		add(lblHola);
		
		JButton btnEntrar = new JButton("Marcar Entrada");
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ManagerSession.saveObject("usuario", user);
				principal.verEntradaPanel();
			}
		});
		btnEntrar.setBounds(36, 127, 120, 98);
		add(btnEntrar);
		
		JButton btnSalir = new JButton("Marcar Salida");
		btnSalir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ManagerSession.saveObject("usuario", user);
				principal.verSalidaPanel();
			}
		});
		btnSalir.setBounds(181, 127, 120, 98);
		add(btnSalir);
		
		JButton btnSalir_3 = new JButton("wip");
		btnSalir_3.setBounds(36, 246, 120, 98);
		add(btnSalir_3);
		
		//mostrar nombre usuario
		
		lblNombreUsuario = new JLabel("nombre usuario");
		lblNombreUsuario.setBounds(68, 17, 143, 15);
		add(lblNombreUsuario);
		
	}

	@Override
	public void init() {
		//recuperar nombre usuario de objeto
		Usuario usuario = (Usuario)ManagerSession.findObject("usuario");
		lblNombreUsuario.setText(usuario.getNombre());
		user=usuario;
		setVisible(true);
	}

}
