package gui;

import javax.swing.JPanel;
import javax.swing.JTextField;

import cl.ufro.controller.UsuarioController;
import cl.ufro.db.Usuario;
import cl.ufro.utils.Imagen;
import cl.ufro.utils.ManagerSession;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.awt.Color;
import cl.ufro.utils.PanelImagen;
import cl.ufro.utils.Serializar;

public class RegistroPanel extends VistaUsuarioPanel {

	private JTextField txtRut;
	private JTextField txtClave;
	private JTextField txtNombre;
	private JTextField txtCorreo;
	private PanelImagen panelImagen;
	private JTextField txtConfirmar;
	private JTextField txtDireccion;
	private JLabel lblMensaje;
	
	/**
	 * Create the panel.
	 */

	public RegistroPanel(Principal principal) {
		super(principal);
		build();
	}
	@Override
	protected void build() {
		JLabel lblRut = new JLabel("Rut:");
		lblRut.setBounds(38, 77, 70, 15);
		add(lblRut);

		JLabel lblClave = new JLabel("Clave:");
		lblClave.setBounds(38, 191, 70, 15);
		add(lblClave);

		txtRut = new JTextField();
		txtRut.setBounds(141, 74, 114, 19);
		add(txtRut);
		txtRut.setColumns(10);

		txtClave = new JTextField();
		txtClave.setBounds(141, 188, 114, 19);
		add(txtClave);
		txtClave.setColumns(10);

		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e1) {
				//Encapsular formulario
				
				Usuario usuario = new Usuario();
				try {
					//condicional para confirmar la clave preferida
					if(txtClave.getText().equals(txtConfirmar.getText())) {
						usuario.setRut(txtRut.getText());
						usuario.setCorreo(txtCorreo.getText());
						usuario.setNombre(txtNombre.getText());
						//usuario.setDireccion(txtDireccion.getText());
						
						int clave = Integer.parseInt(txtClave.getText());
						usuario.setClave(clave);
						
						//byte[]  byteFoto = (byte[])ManagerSession.removeObject("byteFoto");					
						UsuarioController usuarioController = new UsuarioController();
						usuario = usuarioController.registrar(usuario);
						if(usuario!=null) {
							ManagerSession.saveObject("usuario", usuario);
							principal.verHomePanel();
						}else {
							txtRut.setText("");
						}
					}else {
						lblMensaje.setText("Las claves no coinciden, intente otra vez");
					}
				}catch (NumberFormatException e) {
					txtClave.setText("");
				}
			}
		});
		btnRegistrar.setBounds(67, 423, 117, 25);
		add(btnRegistrar);

		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setBounds(38, 106, 70, 15);
		add(lblNombre);

		txtNombre = new JTextField();
		txtNombre.setColumns(10);
		txtNombre.setBounds(141, 103, 114, 19);
		add(txtNombre);

		txtCorreo = new JTextField();
		txtCorreo.setColumns(10);
		txtCorreo.setBounds(141, 132, 114, 19);
		add(txtCorreo);

		JLabel lblCorreo = new JLabel("Correo:");
		lblCorreo.setBounds(38, 132, 70, 15);
		add(lblCorreo);

		JButton btnVolver = new JButton("Volver");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				principal.verLoginPanel();
			}
		});
		btnVolver.setBounds(194, 423, 117, 25);
		add(btnVolver);
		
		JLabel lblFoto = new JLabel("Foto:");
		lblFoto.setBounds(67, 243, 70, 15);
		add(lblFoto);
		
		/*adicion imagen estatica
		 * 
		 * 
		panelImagen = new PanelImagen();
		panelImagen.setBounds(141, 243, 130, 80);
		add(panelImagen);
		
		
		File arch = new File("ruta");
		
		BufferedImage imgOrig =Imagen.readColorImage(arch.getAbsolutePath());
		BufferedImage imgEscalada = Imagen.scale(imgOrig, panelImagen.getWidth(), panelImagen.getHeight());
		panelImagen.setImage(imgEscalada);
		
		
		
		*/
		JButton btnCargar = new JButton("Cargar (wip)");
		btnCargar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				JFileChooser file = new JFileChooser("./");
				file.showOpenDialog(null);
				File arch = file.getSelectedFile();
				if(arch!=null){
					BufferedImage imgOrig =Imagen.readColorImage(arch.getAbsolutePath());
					BufferedImage imgEscalada = Imagen.scale(imgOrig, panelImagen.getWidth(), panelImagen.getHeight());
					panelImagen.setImage(imgEscalada);	
					byte[] byteFoto = Serializar.BufferedImageToBytes(imgOrig);
					ManagerSession.saveObject("byteFoto", byteFoto);
				}
				}
		});
		btnCargar.setBounds(32, 269, 99, 25);
		add(btnCargar);
		
		JLabel lblConfirmeClave = new JLabel("Confirme clave:");
		lblConfirmeClave.setBounds(38, 217, 77, 14);
		add(lblConfirmeClave);
		
		txtConfirmar = new JTextField();
		txtConfirmar.setColumns(10);
		txtConfirmar.setBounds(141, 213, 114, 19);
		add(txtConfirmar);
		
		JLabel lblDireccion = new JLabel("Direccion:");
		lblDireccion.setBounds(38, 165, 70, 15);
		add(lblDireccion);
		
		txtDireccion = new JTextField();
		txtDireccion.setColumns(10);
		txtDireccion.setBounds(141, 158, 114, 19);
		add(txtDireccion);
		
		JLabel lblQr = new JLabel("QR");
		lblQr.setBounds(62, 335, 46, 14);
		add(lblQr);
		
		JButton btnCargar1 = new JButton("Cargar (wip)");
		btnCargar1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser file = new JFileChooser("./");
				file.showOpenDialog(null);
				File arch = file.getSelectedFile();
				if(arch!=null){
					BufferedImage imgOrig =Imagen.readColorImage(arch.getAbsolutePath());
					BufferedImage imgEscalada = Imagen.scale(imgOrig, panelImagen.getWidth(), panelImagen.getHeight());
					panelImagen.setImage(imgEscalada);	
					byte[] byteFoto = Serializar.BufferedImageToBytes(imgOrig);
					ManagerSession.saveObject("byteFoto", byteFoto);
				}
			}
		});
		btnCargar1.setBounds(32, 360, 99, 25);
		add(btnCargar1);
		
		lblMensaje = new JLabel("Mensaje");
		lblMensaje.setBounds(67, 398, 244, 14);
		add(lblMensaje);

	}
	public void init() {
		txtNombre.setText("");
		txtCorreo.setText("");
		txtRut.setText("");
		txtClave.setText("");
		txtConfirmar.setText("");
		txtDireccion.setText("");
		setVisible(true);
	}
}
