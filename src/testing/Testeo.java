package testing;

import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import cl.ufro.*;
import gui.*;
public class Testeo extends JPanel {
	public Testeo() {
	}

	/**
	 * Create the panel.
	 */
	public static void main(String[] args) throws IOException {
		JFrame f= new JFrame("Panel with image");    
		JPanel panel=new JPanel();  
		panel.setLayout(new FlowLayout());      
		BufferedImage myPicture = ImageIO.read(new File(".\\src\\Archivo\\123456789.jpg"));
		JLabel picLabel = new JLabel(new ImageIcon(myPicture));
		panel.add(picLabel);
		f.getContentPane().add(panel);
		f.setSize(500,500);            
		f.setVisible(true); 
	}

}
