package cl.ufro.dao;

import cl.ufro.db.Usuario;
import cl.ufro.utils.ObjetoDao;

public class UsuarioDao extends ObjetoDao{
	
	public UsuarioDao () { };
	
	public Usuario find(Usuario usuario){ 
		return (Usuario)super.find (usuario);
	}
	
}
