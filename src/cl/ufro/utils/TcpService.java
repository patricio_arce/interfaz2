package cl.ufro.utils;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;


public class TcpService {

	private static ServerSocket sersock = null;
	private Socket sock = null;
	private String ip;
	private int port;

	public TcpService(){
		try {
			Properties prop = new Properties();
			InputStream is  = new FileInputStream("config.properties");
			prop.load(is);
			ip = prop.getProperty("ipServidor");
			String puerto = prop.getProperty("puertoServidor");
			port = Integer.parseInt(puerto);
		}catch (Exception e) {
			e.printStackTrace();
		}

	}
	public TcpService(int port){
		try{
			if(sersock == null)
				sersock =  new ServerSocket(port);
		}catch (Exception e) {e.printStackTrace();}
	}
	public TcpService(Socket sock) {
		this.sock=sock;
	}
	public void openConnection(){
		try{
			if(sock == null)
				sock = new Socket(ip, port);
		}catch (Exception e) {e.printStackTrace();}
	}
	public void openConnection(String ip, int port){
		try{
			if(sock == null)
				sock = new Socket(ip, port);
		}catch (Exception e) {e.printStackTrace();}
	}
	public Socket acceptConnection(){
		try{
			closeConnection();
			if(sersock != null) {
				Socket sock = sersock.accept();
				return sock;
			}
		}catch (Exception e) {e.printStackTrace();}
		return null;
	}
	public void closeConnection(){
		try{
			if(sock != null){
				if(!sock.isClosed())
					sock.close();
				sock = null;
			}
		}catch (Exception e) {e.printStackTrace();}
	}
	public void sendObject(Object object){
		try{
			if(sock != null){
				ObjectOutputStream oos = new ObjectOutputStream(sock.getOutputStream());
				oos.writeObject(object);
			}
		}catch (Exception e) {e.printStackTrace();}
	}
	public Object recibeObject(){
		Object object=null;
		try{
			if(sock != null){
				ObjectInputStream ois = new ObjectInputStream(sock.getInputStream());
				object = ois.readObject();
			}
		}catch (Exception e) {e.printStackTrace();}
		return object;
	}

}
