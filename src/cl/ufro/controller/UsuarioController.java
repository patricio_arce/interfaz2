package cl.ufro.controller;

import cl.ufro.db.Usuario;
import cl.ufro.service.UsuarioService;

public class UsuarioController {
	
	private UsuarioService usuarioService = new UsuarioService();
	
	public UsuarioController () { };
	
	public Usuario identificar(Usuario usuario) {
		return usuarioService.identificar(usuario);
	}

	public Usuario registrar(Usuario usuario) {
		return usuarioService.registrar(usuario);		
	}
	
	public Usuario entrar(Usuario usuario, String rutEst) {
		return usuarioService.entrar(usuario, rutEst);
	}
	public Usuario salida(Usuario usuario, String rutEst) {
		return usuarioService.salida(usuario, rutEst);
	}
	
}
