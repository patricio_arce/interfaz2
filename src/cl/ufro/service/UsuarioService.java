package cl.ufro.service;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import cl.ufro.db.Usuario;
import cl.ufro.net.EntradaNet;
import cl.ufro.net.IdentificarNet;
import cl.ufro.net.RegistrarNet;
import cl.ufro.net.SalidaNet;
import cl.ufro.net.SubirFotoNet;
import cl.ufro.utils.TcpService;

public class UsuarioService {
	
	private TcpService tcpService = new TcpService();

	public UsuarioService () { };

	public Usuario identificar(Usuario usuario) {
		try {
			tcpService.openConnection();
			IdentificarNet identificarNet = new IdentificarNet();
			identificarNet.usuario = usuario;
			tcpService.sendObject(identificarNet);
			return (Usuario)tcpService.recibeObject();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public Usuario registrar(Usuario usuario) {
		try {
			tcpService.openConnection();
			RegistrarNet registrarNet = new RegistrarNet();
			registrarNet.usuario = usuario;
			tcpService.sendObject(registrarNet);
			//SubirFotoNet subirFotoNet = new SubirFotoNet();
			//subirFotoNet.img = byteFoto;
			//tcpService.sendObject(subirFotoNet);
			return (Usuario)tcpService.recibeObject();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Usuario entrar(Usuario usuario,String rutEst) {
		try {
			tcpService.openConnection();
			EntradaNet entradaNet =new EntradaNet();
			entradaNet.usuario = usuario;
			entradaNet.rutEstablecimiento = rutEst;
			tcpService.sendObject(entradaNet);
			return (Usuario)tcpService.recibeObject();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}	
	
	public Usuario salida(Usuario usuario,String rutEst) {
		try {
			tcpService.openConnection();
			SalidaNet salidaNet =new SalidaNet();
			salidaNet.usuario = usuario;
			salidaNet.rutEstablecimiento = rutEst;
			tcpService.sendObject(salidaNet);
			return (Usuario)tcpService.recibeObject();
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}