package cl.ufro.db;

import java.util.Date;

import cl.ufro.utils.ObjetoBd;

public class Usuario extends ObjetoBd{

	private String rut;
	private String nombre;
	private int clave;
	private String correo;
	/*private String direccion;
	private String fecha;
	private String registro;
*/
	public Usuario () {
		addToPrimaryKey("rut");
	};/*
	public Usuario (Usuario usuario,String reg) {
		this.setRut(usuario.getRut());
		this.setNombre(usuario.getNombre());
		this.setCorreo(usuario.getCorreo());
		this.setRegistro(reg);
		Date date = new Date();
		this.setFecha(date.toString());
	}
	*/
	public void setRut (String newVar) {
		rut = newVar;
	}
	public String getRut () {
		return rut;
	}
	public void setNombre (String newVar) {
		nombre = newVar;
	}
	public String getNombre () {
		return nombre;
	}
	public void setClave (int newVar) {
		clave = newVar;
	}
	public int getClave () {
		return clave;
	}
	public void setCorreo (String newVar) {
		correo = newVar;
	}
	public String getCorreo () {
		return correo;
	}
	/*
	public void setFecha (String newVar) {
		fecha = newVar;
	}
	public String getFecha () {
		return fecha;
	}
	
	public void setRegistro (String newVar) {
		registro = newVar;
	}
	public String getRegistro () {
		return registro;
	}
	
	
	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String newVar) {
		direccion=newVar;
	}
	*/
}
