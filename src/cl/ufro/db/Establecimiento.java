package cl.ufro.db;

import cl.ufro.utils.ObjetoBd;

public class Establecimiento extends ObjetoBd{

	private String rut;
	private String nombre;
	private int aforo;
	private String PasoaPaso;
	
	public Establecimiento () {
		addToPrimaryKey("rut");
	}

	public String getRut() {
		return rut;
	}

	public void setRut(String rut) {
		this.rut = rut;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getAforo() {
		return aforo;
	}

	public void setAforo(int aforo) {
		this.aforo = aforo;
	}

	public String getPasoaPaso() {
		return PasoaPaso;
	}

	public void setPasoaPaso(String pasoaPaso) {
		PasoaPaso = pasoaPaso;
	};

	

}
